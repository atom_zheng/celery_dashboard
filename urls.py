#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
    Date: 2016/9/12
    Time: 10:49
"""
from __future__ import absolute_import

import os.path

from tornado.web import StaticFileHandler, url

from views.error import NotFoundErrorHandler
from views.dashboard import DashBoardView
from views.test import TestView

settings = dict(
    template_path=os.path.join(os.path.dirname(__file__), 'templates'),
    static_path=os.path.join(os.path.dirname(__file__), 'static'),
    static_url_prefix='/static/',
    login_url='/login',
    debug=True,
)
handlers = [
    url(r'/test', TestView, name="test"),
    url(r"/dashboard", DashBoardView, name="dashboard"),
    #static files
    (r"/static/(.*)", StaticFileHandler, {"path": settings["static_path"]}),
    (r'.*', NotFoundErrorHandler),
]
